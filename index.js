var AWS = require('aws-sdk');
var rekognition = new AWS.Rekognition();
var s3 = new AWS.S3();

exports.handler = function(event, context) {
  
    objectAndSceneDetection();
    faceAnalysis();
    compareFaces('kesha-target.jpg');
    compareFaces('beyonce.jpg');
    compareFaces('oprah.jpg');
    compareFaces('jlo.jpg');
    facialRecognition();
    
    function facialRecognition() {
      var faceRegParams = {
        CollectionId: "rekogPhotos"
      };
 
      //create collection
      rekognition.createCollection(faceRegParams, function(err, data) {
          if (err) 
            console.log(err, err.stack); // an error occurred
          else     
            console.log(data);           // successful response
       });
       
       //add faces to the collection
       
       var listParams = { Bucket: "facematch-rekog"};
       
       //loop through all S3 objects and add to collection by calling index faces
       s3.listObjects(listParams, function(err, data) {
          if (err) return console.error(err);
          
          var i;
          var itemToIndexParams;
          
          // data.Contents is the array of picture objects within the bucket
          for(i=0;i<data.Contents.length;i++) {
           
            itemToIndexParams = {
                CollectionId: "rekogPhotos", 
                DetectionAttributes: [], 
                ExternalImageId: "image" + i, 
                Image: {
                    S3Object: {
                    Bucket: "facematch-rekog", 
                    Name: data.Contents[i].Key
                    }
                }
            };
            
            //add to collection
            rekognition.indexFaces(itemToIndexParams, function(err, data) {
              if (err) 
                console.log(err, err.stack); // an error occurred
              else     
                console.log(data); 
            });
          }
          return; 
       });
       
       //now search the collection for a face
       var faceToSearchParams = {
          CollectionId: "rekogPhotos", 
          FaceMatchThreshold: 95, 
          Image: {
            S3Object: {
              Bucket: "facematch-rekog", 
              Name: "kesha-source.png"
            }
          }, 
          MaxFaces: 5
        };
        
        rekognition.searchFacesByImage(faceToSearchParams, function(err, data) {
            if (err) 
              console.log(err, err.stack); // an error occurred
            else     
              console.log(data);  
        });
      }
    
    function compareFaces(targetName) {
    
      var compareFaceParams = {
       SourceImage: {
        S3Object: {
         Bucket: "facematch-rekog",
         Name: "kesha-source.png"
        }
       },
       TargetImage: {
        S3Object: {
         Bucket: "facematch-rekog",
         Name: targetName
        }
       },
       SimilarityThreshold: 10 //only allow a match of at least 10% match
      };
      
      //Call the service
      rekognition.compareFaces(compareFaceParams, function(error, response) {
        
        if (error) 
         console.log(error, error.stack); // an error occurred
       else {
         if(response.FaceMatches != null && response.FaceMatches.length > 0) {
            console.log("-------- BEGIN: Face Comparison --------");
            console.log("The match with "+ targetName + " is at " + response.FaceMatches[0].Similarity +"%.");
            console.log("-------- END: Face Comparison --------");
            console.log("\n");
          }
        }
      });
    }
    
    function faceAnalysis() {
      var faceParams = {
        Image: {
          S3Object: {
            Bucket: "facematch-rekog",
            Name: "kesha-source.png"
          }
        },
        Attributes: [
            "ALL"
        ]
      };
      
      //Call the service to detect faces
      rekognition.detectFaces(faceParams, function(error, response) {
        if (error) 
          console.log(error, error.stack);
        else {
          console.log("-------- BEGIN: Face Analysis --------");
          var faceDetails = response.FaceDetails[0];
          console.log("Kesha is smiling :"+faceDetails.Smile.Value);
          console.log("Kesha's emotions: "+faceDetails.Emotions[0].Type);
          console.log("Kesha is wearing sunglasses :"+faceDetails.Sunglasses.Value);
          console.log("Kesha's gender :"+faceDetails.Gender.Value);
          console.log("Kesha has beard :"+faceDetails.Beard.Value);
          console.log("Kesha has mustache :"+faceDetails.Mustache.Value);
          console.log("Kesha has opened eyes :"+faceDetails.EyesOpen.Value);
          console.log("-------- END: Face analysis --------");
          console.log("\n");
        }
      });
    }
    
    //Call the obect and scene detection service
    function objectAndSceneDetection() {
      //retrieve target image from S3 bucket
      var landscapeParams = {
          Image: {
            S3Object: {
              Bucket: "facematch-rekog",
              Name: "beach.jpg"
            }
          },
        //only allow a minimum confidence of 90% for any label
        MinConfidence: 90 
      };
      
      var landscapeImage = s3.getObject(landscapeParams);
      
      //Call the detect labels service
      rekognition.detectLabels(landscapeParams, function(error, response) {
        if (error) 
          console.log(error, error.stack); // an error occurred
        else {
          console.log("-------- BEGIN: Object and Scene Detection Output --------");
    
          var labels = response.Labels;
          
          for(var i=0; i<labels.length; i++) {
              console.log("Name ="+labels[i].Name+", Confidence ="+labels[i].Confidence);  
          }
          console.log("-------- END: Object and Scene Detection Output --------");
          console.log("\n");
        }
      });
    }
};
